import React from 'react';
import ReactDOM from 'react-dom';

class BookDetail extends React.Component {
    constructor(props) {
        super(props);
        this.state = {

        }
    }



    render() {
        return (
            <div className="jumbotron">
                <div>Author: {this.props.bookDesc.author}</div>
                <div>Title: {this.props.bookDesc.title}</div>
                <div>Pages: {this.props.bookDesc.pages}</div>
                <div>Genre: {this.props.bookDesc.genre}</div>
            </div>
        );
    }
}

export default BookDetail;