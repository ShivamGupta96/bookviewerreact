import React from 'react';
import ReactDOM from 'react-dom';

class BookItem extends React.Component{
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <button onClick={
                () => {
                    this.props.bookItemDetail(this.props.books);
                }
            }> {this.props.books.title} </button>
        );
    }
}

export default BookItem;