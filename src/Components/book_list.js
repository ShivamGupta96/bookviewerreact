import React from 'react';
import ReactDOM from 'react-dom';
import BookItem from './book_item';

class BookList extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            books: [
                {title: "The Time Machine", author: "H.G Wells", pages: 150, genre: "peace"},
                {title: "Hamlet",author: "William Shakspeare", pages: 200, genre: "drama"},
                {title: "2 States", author: "Chetan Bhgat", pages: 100, genre: "drama"},
                {title: "Gitanjali",author: "Rabindranath Tagore", pages: 50, genre: "Motivaton"}
            ],
            selectedBook: {}
        }
    }

    bookList() {
        let bookItems = [];
        for(let i=0; i<this.state.books.length; i++) {
            bookItems.push(<BookItem
                books={this.state.books[i]}
                bookItemDetail={(itemDetail) => {this.setState({selectedBook: itemDetail})}}
            />);
        }
        console.log(this.state.selectedBook);
        return bookItems;
    }

    render() {
        return (
            <div>
                Book List
                <br/>
                {this.bookList()}
                {this.props.sendBookDetails(this.state.selectedBook)}
            </div>
        );
    }
}

export default BookList;