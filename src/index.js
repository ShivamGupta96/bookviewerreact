import React from 'react';
import ReactDOM from 'react-dom';
import BookDetail from "./Components/book_detail";
import BookList from "./Components/book_list";

class App extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            bookContent: {
                author: "shivam",
                genre: "mystery/thriller"
            }
        }
    }

    render() {
        return (
            <div>
                <h1>Main Component</h1>
                <BookList
                    sendBookDetails={
                        (selectedBookData) => {
                            this.setState({bookContent: selectedBookData});
                        }
                    }
                />
                {console.log(this.state.bookContent)}
                <BookDetail bookDesc={this.state.bookContent}/>
            </div>
        );
    }
}

ReactDOM.render(<App/>, document.querySelector('.container'));